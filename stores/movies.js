import { acceptHMRUpdate, defineStore } from "pinia";

export const useMoviesStore = defineStore(
  "movies",
  () => {
    const config = useRuntimeConfig();
    const movies = ref(null);
    const lastQuery = ref("");

    function getMovieById(imdbID) {
      return movies.value.find((movie) => movie.imdbID === imdbID);
    }

    async function fetchMovies(query) {
      try {
        const data = await $fetch(`${config.public.apiURL}?apikey=${config.public.apiKey}&s=${query}`);

        if (data.Response === "False") {
          throw new Error(data.Error);
        }

        movies.value = data.Search;

        return true;
      } catch (error) {
        throw error;
      }
    }

    return { lastQuery, movies, getMovieById, fetchMovies };
  },
  {
    persist: {
      storage: persistedState.localStorage,
    },
  }
);

if (import.meta.hot) import.meta.hot.accept(acceptHMRUpdate(useMoviesStore, import.meta.hot));
